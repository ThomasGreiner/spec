# Footer for "Gift of ad blocking" campaign

This is a footer strictly used in the [Gift of ad blocking landing page](/spec/adblockplus.org/pages/gift-of-ad-blocking.md).

It's fully identical to the website's [footer](/spec/adblockplus.org/includes/footer.md), with the exception that we need to add an extra link in the [`Legal links` section](/spec/adblockplus.org/includes/footer.md#legal-links) as follows:

``` 
- [Terms of use](terms)
- [Privacy policy](privacy)
- [Campaign privacy policy](privacy-goab)
- [Legal notice](impressum)
```